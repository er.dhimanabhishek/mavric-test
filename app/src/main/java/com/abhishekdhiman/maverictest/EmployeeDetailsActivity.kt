package com.abhishekdhiman.maverictest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_employee_details.*

class EmployeeDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_details)

        val toolbar = findViewById<Toolbar>(R.id.directoryToolbar)
        setSupportActionBar(toolbar)

        setUpEmployeeDetails()
    }

    private fun setUpEmployeeDetails() {
        tvEmpName.text = AppDataRouter.empName
        tvEmpID.text = "6338"
        tvEmpEmail.text = "abc@maveric-systems.com"
        tvEmpmobileNumber.text = "9736961067"
        tvEmpGender.text = "Male"
        tvEmpAddress.text = "#G001, Bangalore"
    }
}
