package com.abhishekdhiman.maverictest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.abhishekdhiman.maverictest.model.ListSearchAdapter
import com.abhishekdhiman.maverictest.model.OnRecyclerItemClickListener
import kotlinx.android.synthetic.main.activity_employee_directory.*

class EmployeeDirectoryActivity : AppCompatActivity() {

    lateinit var predictiveAdapter: ListSearchAdapter
    lateinit var listItems: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_directory)
        val toolbar = findViewById<Toolbar>(R.id.directoryToolbar)
        setSupportActionBar(toolbar)
        listItems = ArrayList()
        listItems.add("Kishan S")
        listItems.add("Harsha")
        listItems.add("Samudyata")
        listItems.add("Sai")
        listItems.add("Roopa")

        rvEmployeeDirectory.verticalScrollbarPosition = View.SCROLLBAR_POSITION_LEFT
        rvEmployeeDirectory.layoutManager = LinearLayoutManager(this)

        predictiveAdapter = ListSearchAdapter(listItems,this, OnRecyclerItemClickListener{position ->
            AppDataRouter.empName = predictiveAdapter.filteredList.get(position)
            val intent = Intent(this,EmployeeDetailsActivity::class.java)
            startActivity(intent)
        })

        predictiveAdapter.setItems(listItems)
        rvEmployeeDirectory.adapter = predictiveAdapter
        predictiveAdapter.notifyDataSetChanged()

        btnSearch.setOnClickListener {
            predictiveAdapter.getFilter().filter(etSearchEmployee.text.toString())
        }

    }
}
