package com.abhishekdhiman.maverictest

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener {
            val username = etUsername.text.toString()
            val pass = etPassword.text.toString()
            if (!username.equals(Constants.USERNAME)){
                showErrroDialog(getString(R.string.invalid_username))
            }else if (!pass.equals(Constants.PASSWORD)){
                showErrroDialog(getString(R.string.invalid_password))
            }else{
                moveToNextScreen()
            }
        }

    }

    private fun moveToNextScreen() {
        val intent  = Intent(this,EmployeeDirectoryActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showErrroDialog(message : String){
        val builder = AlertDialog.Builder(this)
        var alertDialog : AlertDialog? = null
        builder.setTitle(this.getString(R.string.error))
        builder.setMessage(message)
        builder.setPositiveButton(this.getString(R.string.ok)){ dialogInterface, which ->
            etUsername.setText("")
            etPassword.setText("")
            alertDialog?.dismiss()
        }
        alertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

}
