package com.abhishekdhiman.maverictest.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import com.abhishekdhiman.maverictest.R;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ListSearchAdapter extends
        GenericRecyclerViewAdapter<String, OnRecyclerItemClickListener, ListSearchViewHolder> implements Filterable {

    public List<String> filteredList = new ArrayList<>();
    private List<String> fullPredictiveInputList;

    public ListSearchAdapter(List<String> predictivePurposeList, Context context, OnRecyclerItemClickListener listener) {
        super(context, listener);
        fullPredictiveInputList = predictivePurposeList;
        filteredList = fullPredictiveInputList;
    }

    @NotNull
    @Override
    public ListSearchViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list_item, null);
        return new ListSearchViewHolder(view, getListener());
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = fullPredictiveInputList;
                } else {
                    ArrayList<String> dataFilteredList = new ArrayList<>();

                    for (String row : fullPredictiveInputList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.toLowerCase().startsWith(charString.toLowerCase()) || row.contains(charSequence)) {
                            dataFilteredList.add(row);
                        }
                    }
                    filteredList = dataFilteredList;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                setItems(filteredList, true);
            }
        };
    }
}
