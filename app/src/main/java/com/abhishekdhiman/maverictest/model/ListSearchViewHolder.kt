package com.abhishekdhiman.maverictest.model

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.abhishekdhiman.maverictest.R

class ListSearchViewHolder(itemView: View?, listener: OnRecyclerItemClickListener?) :
    BaseViewHolder<String, OnRecyclerItemClickListener?>(itemView, listener) {
    lateinit var tvListItem: TextView
    lateinit var ivArrowExpand: ImageView


    /**
     * Initialize views and set the listeners
     */
    private fun initViews() {
        tvListItem = itemView.findViewById(R.id.tvListItemName)
        ivArrowExpand = itemView.findViewById(R.id.ivArrowRightExpand)
    }

    override fun onBind(item: String) {
        tvListItem.text = item
        if (listener != null) {
            itemView.setOnClickListener { v: View? ->
                listener!!.onItemClick(adapterPosition)
            }
        }
    }


    init {
        initViews()
    }
}
