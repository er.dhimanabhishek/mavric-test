package com.abhishekdhiman.maverictest.model;

import androidx.recyclerview.widget.RecyclerView;

public interface OnRecyclerItemClickListener extends BaseRecyclerListener {

    /**
     * Returns clicked item position {@link RecyclerView.ViewHolder#getAdapterPosition()}
     *
     * @param position clicked item position.
     */
    void onItemClick(int position);
}